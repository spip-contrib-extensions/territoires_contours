# Changelog

## [Unreleased]

## [1.2.0] - 2024-10-12

Avant d'installer cette version il convient absolument de décharger tous les contours et de désinstaller le plugin.

### Changed

- Suivre la logique de Territoires pour le choix des actions du formulaire de chargement
- Changement du préfixe du plugin (voir avertissement)

## [1.1.1] - 2024-07-01

### Fixed

- Correction de l'affichage du formulaire de chargement des contours
- Mise à jour des traductions
- Lien avec la documentation sur Contrib
- Quelques améliorations qualité

## [1.1.0] - 2023-05-28

Cette version est un refactoring significatif du code du plugin qui suit celui du plugin
`Territoires`. Son activation provoquera la suppression de tous les contours déjà chargés qu'il
conviendra de charger à nouveau.

### Added

- Avertir l'utilisateur dans le formulaire de peuplement si le serveur des contours de territoires n'est pas compatible.
- Rangement du formulaire de peuplement par onglets en suivant le plugin `Territoires`.
- Ajout d'une information d'obsolescence d'une unité de peuplement dans le formulaire de peuplement.
- Ajout d'un type de territoire `zone protégée` pour les parcs et réserves naturels.
- Utilisation des fonctions de service fournies par Territoires pour simplifier le code propre.
- Ajout d'un changelog.

### Changed

- Afficher dans la fiche objet d'un territoire son contour sans utiliser le formulaire d'ajout
d'un lien.
- Amélioration du slogan et de la description du plugin.

## [1.0.6] - 2023-05-03

### Changed

- Suivre la décomposition des contours en deux catégories de feeds mise en oeuvre dans le plugin
`Nomenclatures` version 2.0.1.

## [1.0.5] - 2023-04-29

### Changed

- Mise en conformité avec le plugin `Nomenclatures` en version 2.0.0.
- Compatibilité spip 4.2.
- Mise à jour de certaines écritures PHP pour s'adapter à PHP 8.2.
- Actions qualité sur le code : nettoyage, amélioration, typage des fonctions, PHPDoc systématique.
