<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Contours.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible         Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 *
 * @return void
**/
function territoires_contours_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	// Transfert de la meta de peuplement vers 4 metas, une par type
	$maj['2'] = [
		['contours_maj_2']
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 *
 * @return void
**/
function territoires_contours_vider_tables(string $nom_meta_base_version) : void {
	// Récupération des feeds de Contours
	// -- les catégories Nomenclatures concernées
	include_spip('territoires/territoires');
	$categories = territoires_feed_categorie_lister('territoires_contours');
	// -- acquisition brute des feeds
	$feeds = territoires_feed_acquerir('territoires_contours', $categories);

	// Effacer les points GIS et les liens associés en se limitant aux seuls territoires.
	// -- on boucle sur la liste des feeds géométrie plutôt que sur la liste des consignations qui est
	//    plus complexe à parcourir
	include_spip('inc/territoires_contours_unite_peuplement');
	foreach ($feeds as $_id_feed => $_configuration) {
		unite_peuplement_contour_vider($_configuration['tags']['type'], $_configuration['tags']['pays'] ?? '', $_id_feed);
	}

	// Effacer les meta du plugin
	// -- ancienne variable de consignation des peuplements
	effacer_meta('territoires_contours_peuplement');
	// -- nouvelles variables de consignation des peuplements
	include_spip('inc/unite_peuplement');
	include_spip('inc/config');
	$types = lire_config('territoires/types', []);
	foreach ($types as $_type) {
		effacer_meta(unite_peuplement_consigne_identifier('territoires_contours', $_type));
	}
	// -- schéma du plugin
	effacer_meta($nom_meta_base_version);
}

/**
 * Transfert de la meta de peuplement vers 4 metas, une par type et suppression des contours.
 *
 * @return void
**/
function contours_maj_2() : void {
	// Vider les contours existant : il faudra les ajouter de nouveau manuellement.
	// -- on récupère tous les id gis de territoire depuis la table de liens
	$where_lien[] = 'objet=' . sql_quote('territoire');
	$where_gis[] = sql_in_select('id_gis', 'id_gis', 'spip_gis_liens', $where_lien);
	// -- on supprime les liens et les gis
	sql_delete('spip_gis', $where_gis);
	sql_delete('spip_gis_liens', $where_lien);

	// Effacer l'ancienne meta
	effacer_meta('contours_peuplement');
}
