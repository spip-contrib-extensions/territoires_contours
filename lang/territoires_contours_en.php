<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoires_contours-territoires_contours?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'explication_peupler_form' => 'If the territory nomenclatures are loaded, it is possible to associate geographic boundaries with these territories. Emptying boundaries in no way affects territory nomenclatures.',

	// M
	'msg_charger_erreur' => 'An error occurred when populating the "@service@" boundaries.',
	'msg_charger_notice' => 'No update is required for "@service@" boundaries.',
	'msg_charger_succes' => 'The "@service@" boundaries have been loaded.',
	'msg_serveur_erreur' => 'The configured boundary server is not compatible with the current version of the Territory Boundaries plugin.',
	'msg_vider_erreur' => 'An error occurred when emptying the "@service@" boundary.',
	'msg_vider_notice' => 'No emptying is required for "@service@" boundaries.',
	'msg_vider_succes' => 'The "@service@" boundary has been emptied.',

	// T
	'titre_form_peupler' => 'Geographic boundaries of territories',
];
