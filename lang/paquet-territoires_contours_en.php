<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoires_contours-paquet-xml-territoires_contours?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// T
	'territoires_contours_description' => 'This plugin lets you assign GIS contours to your territory objects.',
	'territoires_contours_nom' => 'Territory boundaries',
	'territoires_contours_slogan' => 'Geolocalize territories',
];
