<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires_contours.git

return [

	// E
	'explication_peupler_form' => 'Si les nomenclatures de territoires sont chargées il est possible d’associer des contours géographiques à ces territoires. Le vidage des contours n’affecte en rien les nomenclatures de territoires.',

	// M
	'msg_charger_erreur' => 'Une erreur s’est produite lors du peuplement des contours « @service@ ».',
	'msg_charger_notice' => 'Aucune mise à jour n’est nécessaire pour les contours « @service@ ».',
	'msg_charger_succes' => 'Les contours « @service@ » ont bien été chargées.',
	'msg_serveur_erreur' => 'Le serveur de contours configuré n’est pas compatible avec la version courante du plugin Contours.',
	'msg_vider_erreur' => 'Une erreur s’est produite lors du vidage des contours « @service@ ».',
	'msg_vider_notice' => 'Aucun vidage n’est nécessaire pour les contours « @service@ ».',
	'msg_vider_succes' => 'Les contours « @service@ » ont bien été vidées.',

	// T
	'titre_form_peupler' => 'Contours géographiques des territoires',
];
