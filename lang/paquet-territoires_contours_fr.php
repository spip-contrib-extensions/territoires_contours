<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires_contours.git

return [

	// T
	'territoires_contours_description' => 'Ce plugin permet d’affecter des contours GIS à vos objets territoire.',
	'territoires_contours_nom' => 'Contours des territoires',
	'territoires_contours_slogan' => 'Géolocaliser des territoires',
];
