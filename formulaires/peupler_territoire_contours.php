<?php
/**
 * Gestion du formulaire de chargement ou de vidage des tables de contours.
 *
 * @package    SPIP\TERRITOIRES_CONTOURS\OBJET
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire propose les actions possibles sur les tables de contours,
 * à savoir, charger ou vider et la liste des tables regroupées par service.
 * L'action vider s'appelle décharger car il existe dékà une fonction d'administration de vidage des tables.
 *
 * @uses unite_peuplement_serveur_est_compatible()
 * @uses unite_peuplement_informer_feeds()
 * @uses unite_peuplement_est_chargee()
 * @uses unite_peuplement_est_obsolete()
 *
 * @param string $groupe Groupe de peuplement : représente les types de territoires à présenter dans un même formulaire.
 *
 * @return array Tableau des données à charger par le formulaire (affichage). Aucune donnée chargée n'est un
 *               champ de saisie, celle-ci sont systématiquement remises à zéro.
 *               - `_actions_contour`: (affichage) alias et libellés des actions possibles, `peupler` et `depeupler`
 *               - `_action_defaut`     : (affichage) action sélectionnée par défaut, `peupler`
 *               - `_contours`          : (affichage) la liste des services de géométrie disponibles.
 *               - `_max_choix`         : (affichage) nombre maximum de choix.
 */
function formulaires_peupler_territoire_contours_charger(string $groupe) : array {
	// Initialisation des valeurs à transmettre au formulaire
	$valeurs = [
		'_groupe'          => $groupe,
		'_action_defaut'   => 'charger',
		'_explication'     => '',
		'_attention'       => '',
		'editable'         => true,
	];

	// Vérifier que la version du serveur est compatible avec celle du plugin Territoires.
	// Si ce n'est pas le cas, on affiche un message d'erreur et on rend le formulaire non éditable.
	include_spip('inc/unite_peuplement');
	if (unite_peuplement_serveur_est_compatible('territoires_contours')) {
		// Explication générale du formulaire
		$valeurs['_explication'] = _T('territoires_contours:explication_peupler_form');

		// Lister les actions sur les contours
		$valeurs['_actions_contour'] = [
			'charger' => _T('territoires:option_peupler_action'),
			'vider'   => _T('territoires:option_depeupler_action')
		];

		// Acquérir les services disponibles pour les contours en fonction du groupe
		// -- identifier le type
		if ($groupe === 'zone_country') {
			// Remplissage des données pour le groupe
			$feeds = unite_peuplement_informer_feeds('territoires_contours', 'zone');
			$feeds = array_merge_recursive($feeds, unite_peuplement_informer_feeds('territoires_contours', 'country'));
		} else {
			$feeds = unite_peuplement_informer_feeds('territoires_contours', $groupe);
		}

		// Ranger les services en fonction du type de territoire et du pays.
		include_spip('action/editer_objet');
		$options = ['champ_id' => 'iso_territoire', 'champs' => 'nom_usage'];
		$valeurs['_contours'] = [];
		foreach ($feeds['feed'] as $_id_feed => $_infos) {
			$type = $_infos['tags']['type'];
			$pays = $_infos['tags']['pays'] ?? '';
			if (
				(
					($groupe === 'zone_country')
					and in_array($type, explode('_', $groupe))
				)
				or ($groupe === $type)
			) {
				if (unite_peuplement_est_chargee('territoires', $type, $pays)) {
					// Identification d'un groupe de checkbox si on a besoin de ranger les contours par pays
					if ($groupe === 'zone_country') {
						$groupe_checkbox = '';
					} else {
						$groupe_checkbox = "{$pays} - "
							. extraire_multi(objet_lire('territoire', $pays, $options));
					}
					// Définition de la clé de chaque checkbox et du titre
					$cle = "{$type}:{$pays}:{$_id_feed}";
					$titre = extraire_multi($_infos['title']);
					if (unite_peuplement_est_chargee('territoires_contours', $type, $pays, $_id_feed)) {
						$titre .= ' - <em>['
							. _T('territoires:info_territoire_peuple')
							. (
								unite_peuplement_est_obsolete('territoires_contours', $type, $pays, $_id_feed, $feeds)
									? ', ' . _T('territoires:info_territoire_obsolete')
									: ''
							)
							. ']</em>';
					}

					// Maintenant on range le tableau avec ou sans groupe
					if ($groupe_checkbox) {
						if (!isset($valeurs['_contours'][$groupe_checkbox])) {
							$valeurs['_contours'][$groupe_checkbox] = [];
						}
						$valeurs['_contours'][$groupe_checkbox][$cle] = $titre;
					} else {
						$valeurs['_contours'][$cle] = $titre;
					}
				}
			}
		}
		if ($groupe !== 'zone_country') {
			ksort($valeurs['_contours']);
		}

		// Si aucune unité de peuplement n'est identifiée, alors on rend le formulaire non éditable
		if (!$valeurs['_contours']) {
			$valeurs['editable'] = false;
		}
	} else {
		// Expliquer le problème de compatibilité
		$valeurs['_attention'] = _T('territoires_contours:msg_serveur_erreur');
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

/**
 * Vérification des saisies : il est indispensable de choisir une action (`retirer` ou `ajouter`) et
 * un pays.
 *
 * @param string $groupe Groupe de peuplement : représente les types de territoires à présenter dans un même formulaire.
 *
 * @return array Tableau des erreurs sur l'action et/ou le pays ou tableau vide si aucune erreur.
 */
function formulaires_peupler_territoire_contours_verifier(string $groupe) : array {
	$erreurs = [];

	$obligatoires = ['contours', 'action_contour'];
	foreach ($obligatoires as $_obligatoire) {
		if (!_request($_obligatoire)) {
			$erreurs[$_obligatoire] = _T('info_obligatoire');
		}
	}

	return $erreurs;
}

/**
 * Exécution du formulaire : les pays choisis sont soit vidés, soit chargés.
 *
 * @uses unite_peuplement_contour_charger()
 * @uses unite_peuplement_contour_vider()
 * @uses formulaires_peupler_contours_notifier()
 *
 * @param string $groupe Groupe de peuplement : représente les types de territoires à présenter dans un même formulaire.
 *
 * @return array Tableau retourné par le formulaire contenant toujours un message de bonne exécution ou
 *               d'erreur. L'indicateur editable est toujours à vrai.
 */
function formulaires_peupler_territoire_contours_traiter(string $groupe) : array {
	// Acquisition des saisies: comme elles sont obligatoires, il existe toujours une action et un pays.
	$action = _request('action_contour');
	$contour = _request('contours');

	// On boucle sur tous les ensemble de contours choisis.
	$statuts = [];
	// On identifie exatement le type de territoire et le pays si c'est une subdivision
	[$type, $pays, $service] = explode(':', $contour);

	// Pour chaque pays, on génère l'action demandée.
	// (La fonction de chargement lance un vidage préalable si le pays demandé est déjà chargée)
	include_spip('inc/territoires_contours_unite_peuplement');
	$actionner = "unite_peuplement_contour_{$action}";
	$statuts[] = $actionner($type, $pays, $service);

	// Formater le message correspondant au traitement du type
	$retour = formulaires_peupler_territoire_contours_notifier($action, $statuts);
	$retour['editable'] = true;

	return $retour;
}

/**
 * Formate les messages de succès et d'erreur résultant des actions de chargement ou de vidage
 * des contours.
 *
 * @param string $action  Action venant d'être appliquée à certains pays. Peut prendre les valeurs `peupler` et
 *                        `depeupler`.
 * @param array  $statuts Tableau résultant de l'action sur les tables choisies:
 *                        - `ok`  : `true` si le vidage a réussi, `false` sinon.
 *                        - `nok` : liste des pays en erreur ou tableau vide sinon.
 *                        - `sha` : liste des pays inchangés (SHA identique) ou tableau vide sinon.
 *                        Uniquement disponible pour l'action `peupler`.
 *
 * @return array Tableau des messages à afficher sur le formulaire:
 *               - `message_ok`     : message sur les services ayant été traités avec succès ou tableau vide sinon.
 *               - `message_erreur` : message sur les services en erreur ou tableau vide sinon.
 */
function formulaires_peupler_territoire_contours_notifier(string $action, array $statuts) : array {
	// Initialisation des messages de retour
	$messages = [
		'message_ok'     => '',
		'message_erreur' => ''
	];

	$variables = [
		'ok'  => [],
		'nok' => [],
		'sha' => [],
	];
	$statut_global = [
		'ok'  => false,
		'nok' => false,
		'sha' => false,
	];

	// On compile la liste des pays traités et un indicateur global pour chaque cas d'erreur.
	// Traitement des succès
	foreach ($statuts as $_statut) {
		// Extraction du service
		$service = $_statut['service'];

		// Traitement des retours
		if (!empty($_statut['sha'])) {
			$variables['sha'][] = $service;
			$statut_global['sha'] = true;
		} elseif (!$_statut['ok']) {
			$variables['nok'][] = $service;
			$statut_global['nok'] = true;
		} else {
			$variables['ok'][] = $service;
			$statut_global['ok'] = true;
		}
	}

	// Traitement des succès
	if ($statut_global['ok']) {
		$messages['message_ok'] .= _T("territoires_contours:msg_{$action}_succes", ['service' => implode(', ', $variables['ok'])]);
	}

	// Traitement des erreurs
	if ($statut_global['nok']) {
		$messages['message_erreur'] .= _T("territoires_contours:msg_{$action}_erreur", ['service' => implode(', ', $variables['nok'])]);
	}
	if ($statut_global['sha']) {
		$messages['message_erreur'] .= $messages['message_erreur'] ? '<br />' : '';
		$messages['message_erreur'] .= _T("territoires_contours:msg_{$action}_notice", ['service' => implode(', ', $variables['sha'])]);
	}

	return $messages;
}
