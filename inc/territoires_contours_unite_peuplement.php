<?php
/**
 * API du plugin Contours.
 *
 * @package SPIP\CONTOURS\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajoute des contours géolocalisés aux territoires.
 * Les territoires sont identifiés par groupe via le couple (type, pays).
 *
 * @api
 *
 * @param string $type    Type de territoires à peupler. Prends les valeurs `zone`, `country` ou `subdivision`.
 * @param string $pays    Code ISO 3166-1 alpha2 du pays dont on veut peupler les subdivisions.
 *                        N'est utilisé que si le type choisi est `subdivision`.
 * @param string $service Identifiant du service Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                        la consignation dans le cas des contours uniquement. Sinon vaut chaine vide
 *
 * @return array Tableau retour de la fonction permettant de connaitre le résultat du traitement (utile pour l'affichage
  *               du message dans le formulaire de peuplement).
 */
function unite_peuplement_contour_charger(string $type, string $pays, string $service) : array {
	// On initialise le retour à une erreur nok
	$retour = [
		'ok'      => false,
		'arg'     => false,
		'sha'     => false,
		'type'    => $type,
		'pays'    => $pays,
		'service' => $service
	];

	// Vérifier si l'unité de territoires est à recharger ou pas
	include_spip('inc/unite_peuplement');
	$feeds = unite_peuplement_informer_feeds('territoires_contours', $type, $pays, $service);
	if (unite_peuplement_est_obsolete('territoires_contours', $type, $pays, $service, $feeds)) {
		// Identification de la variable de consigne
		$id_consigne = unite_peuplement_consigne_identifier('territoires_contours', $type, $pays, $service);

		// Détermination du service et acquisition des contours
		$collection = unite_peuplement_acquerir('territoires_contours', $type, $pays, $service, ['cacher_synchrone' => true]);
		if (!empty($collection['contours'])) {
			// On extrait que l'index correspondant aux contours
			$contours = $collection['contours'];

			// On vide les contours, cad les objets GIS et les liens gis-territoire, avant de les remettre
			// (inutile de gérer les erreurs car l'insertion les détectera).
			unite_peuplement_contour_vider($type, $pays, $service);

			// On extrait la liste des codes alternatifs si elle est fournie par l'API.
			// (la liste n'est fournie que si les contours sont liés à un code qui n'est pas celui de base des
			// territoires).
			$codes = [];
			if (!empty($collection['codes_alternatifs'])) {
				$codes = array_column($collection['codes_alternatifs'], 'code_iso', 'code_alter');
			}

			// On insère chaque contour comme un objet GIS et on crée son lien avec le territoire rattaché
			include_spip('action/editer_objet');
			include_spip('action/editer_gis');
			$erreur_insertion = false;
			$nb_contours = 0;
			foreach ($contours as $_contour) {
				// Initialisation de l'enregistrement d'un objet GIS représentant le contour
				$enregistrement = [];

				// Détermination du territoire : si le territoire n'existe pas on n'insère pas le contour
				$id_territoire = 0;
				if ($_contour['code_type'] !== 'code_iso') {
					// La collection est fournie avec la tableau de conversion vers le code iso du territoire
					$code_iso = $codes[$_contour['code']] ?? '';
				} else {
					$code_iso = $_contour['code'];
				}
				if ($code_iso) {
					$options = [
						'champs'   => 'id_territoire',
						'champ_id' => 'iso_territoire'
					];
					$id_territoire = objet_lire('territoire', $code_iso, $options);
				}

				// -- Champs recopiés dans l'objet GIS
				if ($id_territoire) {
					$enregistrement['lat'] = $_contour['lat'];
					$enregistrement['lon'] = $_contour['lon'];
					// -- Champs descriptifs : le titre est composé du service et de l'identifiant pour le service
					$enregistrement['titre'] = "{$service}-{$_contour['code']}";
					$enregistrement['descriptif'] = '';
					// -- Champs de la géométrie : on insère en premier lieu le point central (lat, lon) pour
					//    que la requête fonctionne car le champ geo ne supporte pas d'être vide
					$centre = [
						'type'        => 'Point',
						'coordinates' => [
							(int) ($enregistrement['lon']),
							(int) ($enregistrement['lat'])
						]
					];
					$json = json_encode($centre);
					$geometrie = sql_getfetsel("ST_GeomFromGeoJSON('{$json}')");
					$enregistrement['geo'] = $geometrie;

					// Insertion de l'objet GIS
					if ($id = sql_insertq('spip_gis', $enregistrement)) {
						// Maintenant que l'objet GIS est créé on met à jour sa géométrie avec le contour véritable
						$json = json_encode($_contour['geometry']);
						$geo = ['geo' => "ST_GeomFromGeoJSON('{$json}')"];
						sql_update('spip_gis', $geo, 'id_gis=' . sql_quote($id));

						// Ajout du lien entre le point GIS et le territoire
						gis_associer($id, ['territoire' => $id_territoire]);
						++$nb_contours;
					} else {
						$erreur_insertion = true;
						break;
					}
				} else {
					spip_log("Le territoire (Type '{$type}' - Pays '{$pays}' - Code '{$_contour['code']}') n'a pas de code ISO correspondant", 'territoires_contours' . _LOG_ERREUR);
				}
			}

			if (!$erreur_insertion) {
				// On stocke les informations de chargement dans une meta.
				$contenu = [
					'sha' => $feeds['hash'],
					'nbr' => count($contours),
					'maj' => date('Y-m-d H:i:s'),
					'lic' => $collection['credits'] ?? [],
				];
				include_spip('inc/config');
				ecrire_config($id_consigne, $contenu);
				spip_log("Les '{$nb_contours}' contours (Type '{$type}' - Pays '{$pays}' - Service  '{$service}') ont été chargés", 'territoires_contours' . _LOG_DEBUG);
				$retour['ok'] = true;
			}
		} else {
			$retour['ok'] = false;
			spip_log("Aucun contour pour (Type '{$type}' - Pays '{$pays}' - Service  '{$service}') retourné par Nomenclatures", 'territoires_contours' . _LOG_ERREUR);
		}
	} else {
		$retour['sha'] = true;
		spip_log("Les contours de (Type '{$type}' - Pays '{$pays}' - Service  '{$service}') sont inchangés", 'territoires_contours' . _LOG_AVERTISSEMENT);
	}

	return $retour;
}

/**
 * Supprime des contours géolocalisés de territoires.
 * Les territoires sont identifiés par groupe via le couple (type, pays).
 *
 * @api
 *
 * @param string $type    Type de territoires à peupler. Prends les valeurs `zone`, `country` ou `subdivision`.
 * @param string $pays    Code ISO 3166-1 alpha2 du pays dont on veut peupler les subdivisions.
 *                        N'est utilisé que si le type choisi est `subdivision`, sinon vaut chaine vide.
 * @param string $service Identifiant du service Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                        la consignation dans le cas des contours uniquement. Sinon vaut chaine vide
 *
 * @return array Liste des code ISO 3166-1 alpha2 des pays chargés sous la forme [code] = nom multi.
 */
function unite_peuplement_contour_vider($type, $pays, $service) {
	// On initialise le retour à une erreur nok
	$retour = [
		'ok'      => false,
		'arg'     => false,
		'sha'     => false,
		'type'    => $type,
		'pays'    => $pays,
		'service' => $service
	];

	// Inutile de vider une table vide
	include_spip('inc/unite_peuplement');
	if (unite_peuplement_est_chargee('territoires_contours', $type, $pays, $service)) {
		// Avant de vider la table on réserve la liste des id de territoire qui seront supprimés
		// de façon à vider ensuite les liens éventuels y compris ceux des logos.
		// -- on calcule donc d'emblée la condition IN qui sera appliquée sur la table de liens.
		$from = 'spip_gis';
		$where = [
			'titre LIKE ' . sql_quote("{$service}-%"),
		];
		$where_lien[] = sql_in_select('id_gis', 'id_gis', $from, $where);

		$sql_ok = sql_delete($from, $where);
		if ($sql_ok !== false) {
			// Vider les liens gis_liens
			$where_lien[] = 'objet=' . sql_quote('territoire');
			sql_delete('spip_gis_liens', $where_lien);

			// Supprimer la variable de consignation.
			include_spip('inc/config');
			$id_consigne = unite_peuplement_consigne_identifier('territoires_contours', $type, $pays, $service);
			effacer_config($id_consigne);
		}

		// Enregistrer le succès ou pas du déchargement de la table
		if ($sql_ok !== false) {
			// Succès complet
			$retour['ok'] = true;
			spip_log("Les contours (Type '{$type}' - Pays '{$pays}' - Service  '{$service}') ont été vidés avec succès", 'territoires_contours' . _LOG_DEBUG);
		} else {
			spip_log("Erreur de vidage des contours (Type '{$type}' - Pays '{$pays}' - Service  '{$service}')", 'territoires_contours' . _LOG_ERREUR);
		}
	} else {
		$retour['sha'] = true;
		spip_log("Aucun contour (Type '{$type}' - Pays '{$pays}' - Service  '{$service}') à vider", 'territoires_contours' . _LOG_AVERTISSEMENT);
	}

	return $retour;
}
