<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajout de contenu sur la partie centrale, en fin de page.
 * Contours rajoute un formulaire de peuplement des contours dans la page de peuplement de Territoires.
 *
 * @pipeline affiche_milieu
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline modifiées pour refléter le traitement.
 */
function territoires_contours_affiche_milieu(array $flux) : array {
	if ($flux['args']['exec'] === 'peupler_territoires') {
		// Par défaut, le groupe est subdivision
		$groupe = _request('groupe') ?? 'subdivision';
		if (
			$groupe
			and ($groupe !== 'infrasubdivision')
		) {
			$flux['data'] .= recuperer_fond(
				'prive/squelettes/inclure/inc-peupler_territoire_contours',
				['groupe' => $groupe]
			);
		}
	}

	return $flux;
}

/**
 * Insertion dans le bloc wysiwyg d'une fiche objet.
 * Le plugin Contours ajoute la carte du territoire si elle existe sans référence à l'objet GIS.
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline modifiées pour refléter le traitement.
 */
function territoires_contours_afficher_contenu_objet($flux) {
	// Fiche objet territoire
	if (
		($objet = $flux['args']['type'])
		and ($objet === 'territoire')
		and ($id_objet = (int) ($flux['args']['id_objet']))
	) {
		$texte = recuperer_fond(
			'prive/squelettes/inclure/inc-territoire_contour',
			[
				'id_objet' => $id_objet
			]
		);
		$flux['data'] .= $texte;
	}

	return $flux;
}

/**
 * Complément à la fonction de peuplement des territoires.
 *
 * Contours rétablit les éventuels liens entre les territoires concernés et les contours GIS qui existaient
 * avant le chargement (spip_gis_liens).
 *
 * @pipeline post_peupler_territoire
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline telles que reçues.
 */
function territoires_contours_post_peupler_territoire(array $flux) : array {
	// Rétablir les liens avec les objets GIS
	if (
		!empty($flux['args']['sauvegardes'])
		and ($sauvegardes = $flux['args']['sauvegardes'])
		and !empty($flux['args']['sauvegardes']['gis'])
		and !empty($flux['args']['ids_crees'])
	) {
		include_spip('inc/unite_peuplement');
		$config_lien = [
			'table'    => 'spip_gis_liens',
			'id_table' => 'id_objet'
		];
		unite_peuplement_retablir_liens(
			'gis',
			$sauvegardes,
			$flux['args']['ids_crees'],
			$config_lien
		);
	}

	return $flux;
}

/**
 * Complément à la fonction de préservation des liens et des éditions de territoires.
 *
 * Contours sauvegarde les éventuels liens entre les territoires concernés et des contours GIS (spip_gis_liens).
 *
 * @pipeline post_preserver_territoire
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline modifiées en rajoutant la liste des liens à préserver.
 */
function territoires_contours_post_preserver_territoire(array $flux) : array {
	// Extraction de la correspondance id-iso pour les liens vers les objets GIS
	if (
		!empty($flux['args']['type'])
		and ($type = $flux['args']['type'])
		and isset($flux['args']['pays'])
		and ($pays = $flux['args']['type'])
	) {
		// Extraction des liens de contours GIS vers les territoires concernés
		$from = 'spip_territoires';
		$where = [
			'type=' . sql_quote($type),
		];
		if ($type === 'subdivision') {
			$where[] = 'iso_pays=' . sql_quote($pays);
		}
		$where_gis = [
			'objet=' . sql_quote('territoire'),
			sql_in_select('id_objet', 'id_territoire', $from, $where)
		];
		$flux['data']['gis'] = sql_allfetsel('*', 'spip_gis_liens', $where_gis);

		// Construire le tableau de correspondance id-iso pour les territoires concernés
		$select = ['id_territoire', 'iso_territoire'];
		$where_ids = [
			sql_in('id_territoire', array_column($flux['data']['gis'], 'id_objet'))
		];
		if ($ids = sql_allfetsel($select, $from, $where_ids)) {
			$ids = array_column($ids, 'id_territoire', 'iso_territoire');
			$flux['data']['ids'] = array_merge($flux['data']['ids'], $ids);
		}
	}

	return $flux;
}

/**
 * Complément à la fonction de dépeuplement des territoires.
 *
 * Contours supprime les liens éventuels entre les territoires concernés et des objets GIS (spip_gis_liens).
 *
 * @pipeline post_depeupler_territoire
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline telles que reçues.
 */
function contours_post_depeupler_territoire(array $flux) : array {
	// Vider les liens éventuels avec les objets GIS
	if (
		!empty($flux['args']['ids_territoire'])
		and ($ids = $flux['args']['ids_territoire'])
	) {
		$where_gis = [
			sql_in('id_objet', $ids),
			'objet=' . sql_quote('territoire')
		];
		sql_delete('spip_gis_liens', $where_gis);
	}

	return $flux;
}

/**
 * Déclaration des arguments supplémentaires utilisables dans le modele pour gis.
 *
 * Les arguments iso_parent, iso_pays pourront être passés au modèle.
 *
 * @param array $flux Arguments acceptés pour le modèle
 *
 * @return array Arguments acceptés pour le modèle complétés.
 */
function contours_gis_modele_parametres_autorises(array $flux) : array {
	// Ajout des codes ISO du pays et du parent dans les paramètres du modèle GIS.
	$flux[] = 'iso_pays';
	$flux[] = 'iso_parent';

	return $flux;
}
