# Plugin Contours des Territoires

## Introduction

L’objectif du plugin est de fournir les contours géographiques des objets «territoire». Ces contours sont matérialisés par des objets GIS.
Le plugin fournit une interface de peuplement des données à partir de l’API REST du plugin Nomenclatures. Cette interface s'intègre totalement dans celle du plugin Territoires.

## Documentation

La documentation utilisateur du plugin est disponible sur [SPIP-Contrib](https://contrib.spip.net/Contours-des-Territoires-utilisation-du-plugin).

## Installation

Le plugin s’installe comme n’importe quel plugin SPIP. Il nécessite les plugins Territoires, GIS Géométrie et Vérifier qui eux mêmes nécessitent d'autres plugins utiles à Contours des Territoires.

## L’interface utilisateur

L’interface utilisateur fournit par le plugin Contours des Territoires ne concerne que l’espace privé. Elle permet, au travers de plusieurs pages d’assurer les fonctions de consultation et de peuplement des contours. Ces compléments s'intègrent dans les pages existantes du plugin Territoires.

### Page liste des territoires

Aucune modification n'y est apportée.

### Page peuplement des territoires

Le plugin Contours des territoires rajoute, pour chaque onglet compatible, un formulaire de chargement des contours disponibles pour les types de territoire concernés.

Ce formulaire est présenté sous le formulaire de chargement des territoires. Les contours ne sont présentés au chargement que si les territoires associés sont déjà chargés.
Actuellement, aucun contour n'est disponible pour les infra-subdivisions.

### Page configuration du plugin

Aucune modification n'y est apportée.

### Page d’un objet territoire

La page `territoire` affiche la fiche objet de chaque territoire. Le plugin Contours des Territoires rajoute, en fin de la fiche objet du territoire, une fenêtre
avec le contour concerné appliqué à la carte standard du monde fournie par défaut par le plugin GIS.

## Plugins connexes

Le plugin Contours des Territoires tire l'ensemble de ses données du plugin {{Nomenclatures}} (préfixe <code>isocode</code>) qui offre une API REST pour un certain nombre de données normalisées de type géographique ou linguistique. Pour ajouter des contours non encore disponibles dans le plugin il faut en fait les rendre disponibles dans le plugin Nomenclatures.
De fait, ces contours seront automatiquement détectés par le plugin Contours des Territoires et proposés au chargement.

Le plugin Contours des Territoires sert aussi de base au plugin Cartes des territoires qui permet de regrouper des Territoires afin
d'en faire des cartes géographiques.
