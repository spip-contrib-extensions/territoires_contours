<?php
/**
 * Ce fichier contient les fonctions de service spécifiques du plugin Contours et nécessitées par
 * l'utilisation du plugin `Territoires`.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


if (!defined('_TERRITOIRES_CONTOURS_URL_BASE_ISOCODE')) {
	/**
	 * Endpoint de l'API REST hébergeant les données de Nomenclatures.
	 */
	define('_TERRITOIRES_CONTOURS_URL_BASE_ISOCODE', 'https://contrib.spip.net/http.api/ezrest');
}

if (!defined('_TERRITOIRES_CONTOURS_COMPATIBILITE_ISOCODE')) {
	/**
	 * Endpoint de l'API REST hébergeant les données de Nomenclatures.
	 */
	define('_TERRITOIRES_CONTOURS_COMPATIBILITE_ISOCODE', ['vmin' => '2.0.1']);
}

// -----------------------------------------------------------------------
// ----------- SERVICES RELATIFS AUX SERVEURS DE PEUPLEMENT --------------
// -----------------------------------------------------------------------

/**
 * Renvoie, pour le plugin appelant, les bornes de compatibilité avec le serveur fournissant une nature d'informations
 * donnée sur les territoires.
 * En fait, la nature d'information est liée au plugin appelant (nomenclatures pour Territoires, contours géographiques
 * pour le plugin Contours de territoires, etc.).
 *
 * @uses territoires_chercher_service()
 *
 * @return array Bornes de compatibilité avec le serveur.
 */
function territoires_contours_serveur_definir_compatibilite() : array {
	// Renvoyer les bornes de compatibilité
	return _TERRITOIRES_CONTOURS_COMPATIBILITE_ISOCODE;
}

// -----------------------------------------------------------------------
// ------------------- SERVICES RELATIFS AUX FEEDS -----------------------
// -----------------------------------------------------------------------

/**
 * Renvoie l'URL de base du serveur Nomenclatures fournissant des informations sur les territoires.
 *
 * @return string URL de base du serveur REST
 */
function territoires_contours_feed_initialiser_url_base() : string {
	// Territoires utilise par défaut l'URL de Contrib
	return _TERRITOIRES_CONTOURS_URL_BASE_ISOCODE;
}

/**
 * Renvoie la liste des catégories de feed de Nomenclatures correspondant à la nature des informations demandées.
 *
 * @return array Liste des catégories de feeds de Nomenclatures utilisées par le plugin appelant.
 */
function territoires_contours_feed_categorie_lister() : array {
	// Les contours coincident avec les catégories de feed map et map_pa.
	return ['map', 'map_pa'];
}

/**
 * Renvoie, pour la collection `feeds`, l'index du hash permettant de savoir si le ou les feeds récupérés sont
 * obsolètes.
 *
 * @param string $type Type de territoires. Prends les valeurs `zone`, `country`, `subdivision`, `protected_area` ou `infrasubdivision`.
 *
 * @return string Index du hash dans la réponse à la requête REST.
 */
function territoires_contours_feed_indexer_hash(string $type) : string {
	// Les feeds de contours sont toujours organisés par pays : un seul pays par feed.
	// L'index du hash est donc toujours le même, 'hash'.
	return 'hash';
}

// -----------------------------------------------------------------------
// ------------ SERVICES RELATIFS AUX UNITES DE PEUPLEMENT ---------------
// -----------------------------------------------------------------------

/**
 * Renvoie, la collection de Nomenclatures à requêter en fonction du type de territoires concerné.
 * Pour le plugin Contours, seule la collection `contours` est utilisée.
 *
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision`, `protected_area` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                             la consignation dans le cas des contours uniquement. Sinon vaut chaine vide.
 *
 * @return string Identifiant de la collection.
 */
function territoires_contours_unite_peuplement_definir_collection(string $type, ?string $pays = '', ?string $service = '') : string {
	// Quelque soit le type de territoire, la collection est toujours 'contours'
	return 'contours';
}

/**
 * Renvoie, les filtres à appliquer à la requête Nomenclatures en fonction du type de territoires, du pays voire
 * du service concernés.
 *
 * @param string      $type    Type de territoires. Prends les valeurs `zone`, `country`, `subdivision`, `protected_area` ou `infrasubdivision`.
 * @param null|string $pays    Code ISO 3166-1 alpha2 du pays si le type est `subdivision` ou `infrasubdivision` sinon une chaine vide.
 * @param null|string $service Identifiant du feed Nomenclatures ayant permis le chargement. Est utilisé pour indexer
 *                             la consignation dans le cas des contours uniquement. Sinon vaut chaine vide.
 *
 * @return array Filtres à appliquer à la collection.
 */
function territoires_contours_unite_peuplement_definir_filtre(string $type, ?string $pays = '', ?string $service = '') : array {
	// Pour une requête il faut préciser jusqu'au service pour éviter de récupérer des volumes trop importants
	// de données géographiques
	$filtres = [];
	if ($type) {
		$filtres['type'] = $type;
	}
	if ($pays) {
		$filtres['pays'] = $pays;
	}
	if ($service) {
		$filtres['service'] = $service;
	}

	return $filtres;
}
